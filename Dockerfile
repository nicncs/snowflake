# This file is a template, and might need editing before it works on your project.
FROM node:10

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

# Installing dependencies
COPY package*.json ./
RUN npm install -g yarn

# Copying source files
COPY . .

# Building app
RUN yarn
RUN yarn build

# replace this with your application's default port
EXPOSE 3000

# Running the app
CMD [ "yarn", "start" ]
